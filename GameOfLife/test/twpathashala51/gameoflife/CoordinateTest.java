package twpathashala51.gameoflife;

import com.sun.javaws.exceptions.InvalidArgumentException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class CoordinateTest {

    @Test
    public void moveLeft() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(0,1).equals(currentLocation.left()));
    }

    @Test
    public void moveRight() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(2,1).equals(currentLocation.right()));
    }

    @Test
    public void moveDown() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(1,0).equals(currentLocation.down()));
    }

    @Test
    public void moveUp() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(1,2).equals(currentLocation.up()));
    }

    @Test
    public void moveTopLeft() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(0,2).equals(currentLocation.topLeft()));
    }

    @Test
    public void moveTopRight() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(2,2).equals(currentLocation.topRight()));
    }

    @Test
    public void moveBottomRight() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(2,0).equals(currentLocation.bottomRight()));
    }

    @Test
    public void moveBottomLeft() {
        Coordinate currentLocation = new Coordinate(1,1);
        Assert.assertTrue(new Coordinate(0,0).equals(currentLocation.bottomLeft()));
    }

    @Test
    public void shouldGive2LiveNeighboursForCellAt1coma2(){
        Board board = BoardWithLiveCells2coma1and2coma2();
        Assert.assertEquals(2, new Coordinate(1,2).liveNeighbours(board));
    }

    @Test
    public void shouldGive1LiveNeighboursForCellAt1coma0(){
        Board board = BoardWithLiveCells2coma1and2coma2();
        Assert.assertEquals(1, new Coordinate(1,0).liveNeighbours(board));
    }

    private Board BoardWithLiveCells2coma1and2coma2() {
        HashSet<Coordinate> liveCells = new HashSet<>();
        Coordinate liveOne = new Coordinate(2,1);
        Coordinate liveTwo = new Coordinate(2,2);
        liveCells.add(liveOne);
        liveCells.add(liveTwo);
        return new Board(liveCells);
    }
}
