package twpathashala51.gameoflife;

import com.sun.javaws.exceptions.InvalidArgumentException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;


public class BoardTest {

    @Test
    public void findLiveCellIfPresent() throws InvalidArgumentException {
        HashSet<Coordinate> liveCells = new HashSet<>();
        final Coordinate cell = new Coordinate(0, 0);
        liveCells.add(cell);
        Board board = new Board(liveCells);
        Assert.assertTrue(board.isAlive(cell));
    }

    @Test
    public void canNotLiveCellWhenAbsent() throws InvalidArgumentException {
        HashSet<Coordinate> liveCells = new HashSet<>();
        final Coordinate cell = new Coordinate(0, 0);
        liveCells.add(cell);
        Board board = new Board(liveCells);
        Assert.assertFalse(board.isAlive(new Coordinate(2, 3)));
    }

    @Test
    public void findAloneCellsInDefaultSetOne() {
        Board board = new Board(inputCellsSetOne());
        HashSet<Coordinate> output = board.findAloneCells();
        Assert.assertTrue(output.equals(aloneCellsInBoardSetOne()));
    }

    @Test
    public void findAloneCellsInDefaultSetTwo() {
        Board board = new Board(inputCellsSetTwo());
        HashSet<Coordinate> output = board.findAloneCells();
        Assert.assertTrue(output.equals(aloneCellsInBoardSetTwo()));
    }

    @Test
    public void findAllCrowdedCellsInBoardThree() {
        Board board = new Board(inputCellsSetThree());
        HashSet<Coordinate> output = board.findCrowdedCells();
        Assert.assertTrue(output.equals(crowdedCellsInBoardThree()));
    }

    @Test
    public void findAllCrowdedCellsInBoardFour() {
        Board board = new Board(inputCellsSetFour());
        HashSet<Coordinate> output = board.findCrowdedCells();
        Assert.assertTrue(output.equals(crowdedCellsInBoardFour()));
    }

    @Test
    public void findDeadCellForIncarnationInBoardOne() {
        Board board = new Board(inputCellsSetOne());
        HashSet<Coordinate> output = board.deadCellsTobeReincarnate();
        HashSet<Coordinate> toBeIncarnatedCellsForSetOne = new HashSet<>();
        toBeIncarnatedCellsForSetOne.add(new Coordinate(1, 1));
        Assert.assertTrue(output.equals(toBeIncarnatedCellsForSetOne));
    }

    @Test
    public void findDeadCellForIncarnationInBoardTwo() {
        Board board = new Board(inputCellsSetTwo());
        HashSet<Coordinate> output = board.deadCellsTobeReincarnate();
        Assert.assertTrue(output.equals(toBeReincarnateCellsForBoardTwo()));
    }

    private HashSet<Coordinate> toBeReincarnateCellsForBoardTwo() {
        HashSet<Coordinate> output = new HashSet<>();
        output.add(new Coordinate(0, 1));
        output.add(new Coordinate(2, 1));
        return output;
    }

    @Test
    public void gameOfLife() {
        Board inputBoard = new Board(inputCellsSetOne());
        Board output = inputBoard.gameOfLife();
        Assert.assertTrue(output.equals(new Board(gameOfLifeOutput())));
    }

    private HashSet<Coordinate> gameOfLifeOutput() {
        HashSet<Coordinate> expectedLiveCells = new HashSet<>();
        expectedLiveCells.add(new Coordinate(2, 1));
        expectedLiveCells.add(new Coordinate(1, 1));
        return expectedLiveCells;
    }

    private HashSet<Coordinate> inputCellsSetOne() {
        HashSet<Coordinate> inputs = new HashSet<>();
        inputs.add(new Coordinate(2, 1));
        inputs.add(new Coordinate(2, 2));
        inputs.add(new Coordinate(1, 0));
        return inputs;
    }

    public HashSet<Coordinate> inputCellsSetTwo() {
        HashSet<Coordinate> inputs = new HashSet<>();
        inputs.add(new Coordinate(1, 0));
        inputs.add(new Coordinate(1, 1));
        inputs.add(new Coordinate(1, 2));
        return inputs;
    }

    public HashSet<Coordinate> inputCellsSetFour() {
        HashSet<Coordinate> inputs = inputCellsSetTwo();
        inputs.add(new Coordinate(0, 1));
        inputs.add(new Coordinate(2, 1));
        return inputs;
    }

    public HashSet<Coordinate> inputCellsSetThree() {
        HashSet<Coordinate> input = inputCellsSetOne();
        input.add(new Coordinate(1, 1));
        input.add(new Coordinate(0, 1));
        input.add(new Coordinate(0, 0));
        return input;
    }

    private HashSet<Coordinate> aloneCellsInBoardSetOne() {
        HashSet<Coordinate> expectedLiveCells = new HashSet<>();
        expectedLiveCells.add(new Coordinate(1, 0));
        expectedLiveCells.add(new Coordinate(2, 2));
        return expectedLiveCells;
    }

    private HashSet<Coordinate> aloneCellsInBoardSetTwo() {
        HashSet<Coordinate> expectedLiveCells = new HashSet<>();
        expectedLiveCells.add(new Coordinate(1, 0));
        expectedLiveCells.add(new Coordinate(1, 2));
        return expectedLiveCells;
    }

    private HashSet<Coordinate> crowdedCellsInBoardThree() {
        HashSet<Coordinate> expectedLiveCells = new HashSet<>();
        expectedLiveCells.add(new Coordinate(1, 1));
        expectedLiveCells.add(new Coordinate(1, 0));
        return expectedLiveCells;
    }


    private HashSet<Coordinate> crowdedCellsInBoardFour() {
        HashSet<Coordinate> output = new HashSet<>();
        output.add(new Coordinate(1, 1));
        return output;
    }
}