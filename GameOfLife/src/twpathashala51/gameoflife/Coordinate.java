package twpathashala51.gameoflife;

public class Coordinate {

    final private int x;
    final private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordinate left() {
        return new Coordinate(x-1,y);
    }

    public Coordinate right() {
        return new Coordinate(x+1,y);
    }

    public Coordinate down() {
        return new Coordinate(x,y-1);
    }

    public Coordinate up() {
        return new Coordinate(x,y+1);
    }

    public Coordinate topLeft() {
        return new Coordinate(x-1,y+1);
    }

    public Coordinate topRight() {
        return new Coordinate(x+1,y+1);
    }

    public Coordinate bottomRight() {
        return new Coordinate(x+1,y-1);
    }

    public Coordinate bottomLeft() { return new Coordinate(x-1,y-1); }

    public int liveNeighbours(final Board board) {
        int nei = 0;
        if(board.isAlive(this.right())) {
            nei += 1;
        }
        if(board.isAlive(this.left())) {
            nei += 1;
        }
        if(board.isAlive(this.up())) {
            nei += 1;
        }
        if(board.isAlive(this.down())) {
            nei += 1;
        }
        if(board.isAlive(this.topLeft())) {
            nei += 1;
        }
        if(board.isAlive(this.topRight())) {
            nei += 1;
        }
        if(board.isAlive(this.bottomLeft())) {
            nei += 1;
        }
        if(board.isAlive(this.bottomRight())) {
            nei += 1;
        }
        return nei;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinate that = (Coordinate) o;

        if (x != that.x) return false;
        return y == that.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString(){
        return "("+this.x+", "+this.y+")";
    }
}
