package twpathashala51.gameoflife;

import java.util.HashSet;
import java.util.Set;

public class Board {

  public static final int CRITERIA_FOR_ALONE = 2;
  private static final int CRITERIA_FOR_CROWDED = 3;
  private static final int CRITERIA_FOR_REINCARNATION = 3;

  private HashSet<Coordinate> liveCells;

  public Board(HashSet<Coordinate> liveCells) {
    this.liveCells = new HashSet<>();
    for (Coordinate cell : liveCells) {
      this.liveCells.add(cell);
    }
  }

  public Board gameOfLife() {
    Set finalOutput = new HashSet();
    finalOutput.addAll(liveCells);
    finalOutput.removeAll(findAloneCells());
    finalOutput.removeAll(findCrowdedCells());
    finalOutput.addAll(deadCellsTobeReincarnate());
    return new Board((HashSet<Coordinate>) finalOutput);
  }

  public boolean isAlive(Coordinate cell) {
    if (liveCells.contains(cell)) return true;
    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Board board = (Board) o;

    return liveCells != null ? liveCells.equals(board.liveCells) : board.liveCells == null;

  }

  @Override
  public int hashCode() {
    return liveCells != null ? liveCells.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Board{" +
        "liveCells=" + liveCells +
        '}';
  }

  public HashSet<Coordinate> findAloneCells() {
    HashSet<Coordinate> aloneCells = new HashSet<>();
    for (Coordinate cell: liveCells) {
      if(cell.liveNeighbours(this) < CRITERIA_FOR_ALONE){
        aloneCells.add(cell);
      }
    }
    return aloneCells;
  }

  public HashSet<Coordinate> findCrowdedCells() {
    HashSet<Coordinate> crowdedCells = new HashSet<>();
    for(Coordinate cell: liveCells){
      if(cell.liveNeighbours(this) > CRITERIA_FOR_CROWDED) {
        crowdedCells.add(cell);
      }
    }
    return crowdedCells;
  }

  public HashSet<Coordinate> deadCellsTobeReincarnate() {
      HashSet<Coordinate> deadCellsWithAtLeastOneLiveNeighbour = getLiveCellsWithAtLeastOneLiveNeighbour();
      HashSet<Coordinate> deadCellsTobeIncarnated = new HashSet<>();
      for(Coordinate cell: deadCellsWithAtLeastOneLiveNeighbour){
        if(cell.liveNeighbours(this) == CRITERIA_FOR_REINCARNATION) deadCellsTobeIncarnated.add(cell);
      }
    return deadCellsTobeIncarnated;
  }

  private HashSet<Coordinate> getLiveCellsWithAtLeastOneLiveNeighbour() {
    HashSet<Coordinate> output = new HashSet<>();
    for (Coordinate cell: liveCells) {
      if(!isAlive(cell.left())) output.add(cell.left());
      if(!isAlive(cell.right())) output.add(cell.right());
      if(!isAlive(cell.up())) output.add(cell.up());
      if(!isAlive(cell.down())) output.add(cell.down());
      if(!isAlive(cell.topLeft())) output.add(cell.topLeft());
      if(!isAlive(cell.topRight())) output.add(cell.topRight());
      if(!isAlive(cell.bottomRight())) output.add(cell.bottomRight());
      if(!isAlive(cell.bottomLeft())) output.add(cell.bottomLeft());
    }
    return output;


  }
}
