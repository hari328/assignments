package twpathashala51.gameoflife;

import java.util.HashSet;

public class Game {

  public static void main(String[] args) {
      runGameOfLife("Set1", blockPattern(), blockPatternOutput());
      runGameOfLife("Set2", boatPattern(), boatPatternOutput());
      runGameOfLife("Set3", blinketEyePattern(), blinkerEyeOutput());
      runGameOfLife("Set4", toadPattern(), toadPatternOutput());
    }

  private static void runGameOfLife(String inputName, Board input, HashSet<Coordinate> outputSet) {
    Board boardOneOutput;
    boardOneOutput = input.gameOfLife();
    if (! boardOneOutput.equals(new Board(outputSet))) {
      System.out.printf("Failed For %s\n%n", inputName);
      return;
    }

    System.out.println(inputName);
    System.out.println(input);
    System.out.println("output of above cells");
    System.out.println(boardOneOutput);
  }

  private static Board blockPattern() {
    HashSet<Coordinate> inputCoordinates = new HashSet<>();
    inputCoordinates.add(new Coordinate(1, 1));
    inputCoordinates.add(new Coordinate(2, 1));
    inputCoordinates.add(new Coordinate(1, 2));
    inputCoordinates.add(new Coordinate(2, 2));
    return new Board(inputCoordinates);

  }

  private static HashSet<Coordinate> blockPatternOutput() {
    HashSet<Coordinate> outputCoordiates = new HashSet<>();
    outputCoordiates.add(new Coordinate(1, 1));
    outputCoordiates.add(new Coordinate(2, 1));
    outputCoordiates.add(new Coordinate(1, 2));
    outputCoordiates.add(new Coordinate(2, 2));
    return outputCoordiates;
  }

  private static Board boatPattern() {
    HashSet<Coordinate> inputCoordinates = new HashSet<>();
    inputCoordinates.add(new Coordinate(0, 1));
    inputCoordinates.add(new Coordinate(1, 0));
    inputCoordinates.add(new Coordinate(2, 1));
    inputCoordinates.add(new Coordinate(0, 2));
    inputCoordinates.add(new Coordinate(1, 2));
    return new Board(inputCoordinates);

  }

  private static HashSet<Coordinate> boatPatternOutput() {
    HashSet<Coordinate> outputCoordiates = new HashSet<>();
    outputCoordiates.add(new Coordinate(0, 1));
    outputCoordiates.add(new Coordinate(1, 0));
    outputCoordiates.add(new Coordinate(2, 1));
    outputCoordiates.add(new Coordinate(0, 2));
    outputCoordiates.add(new Coordinate(1, 2));
    return outputCoordiates;
  }


  private static Board blinketEyePattern() {
    HashSet<Coordinate> inputCoordinates = new HashSet<>();
    inputCoordinates.add(new Coordinate(1, 1));
    inputCoordinates.add(new Coordinate(1, 0));
    inputCoordinates.add(new Coordinate(1, 2));
    return new Board(inputCoordinates);
    }

  private static HashSet<Coordinate> blinkerEyeOutput() {
    HashSet<Coordinate> outputCoordiates = new HashSet<>();
    outputCoordiates.add(new Coordinate(1, 1));
    outputCoordiates.add(new Coordinate(0, 1));
    outputCoordiates.add(new Coordinate(2, 1));
    return outputCoordiates;
  }

  private static Board toadPattern() {
    HashSet<Coordinate> inputCoordinates = new HashSet<>();
    inputCoordinates.add(new Coordinate(1, 1));
    inputCoordinates.add(new Coordinate(1, 2));
    inputCoordinates.add(new Coordinate(1, 3));
    inputCoordinates.add(new Coordinate(2, 2));
    inputCoordinates.add(new Coordinate(2, 3));
    inputCoordinates.add(new Coordinate(2, 4));
    return new Board(inputCoordinates);
  }

  private static HashSet<Coordinate> toadPatternOutput() {
    HashSet<Coordinate> outputCoordiates = new HashSet<>();
    outputCoordiates.add(new Coordinate(0, 2));
    outputCoordiates.add(new Coordinate(1, 1));
    outputCoordiates.add(new Coordinate(1, 4));
    outputCoordiates.add(new Coordinate(2, 1));
    outputCoordiates.add(new Coordinate(2, 4));
    outputCoordiates.add(new Coordinate(3, 3));
    return outputCoordiates;
  }

}


