package pathashala51;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class BoardTest {

  @Test
  public void shouldTellIfGivenCoordinateIsMine() throws MineOutOfBoardException {
    Board board = new Board(3, new Coordinate(2,2));
    assertFalse(board.notMine(new Coordinate(2,2)));
  }

  @Test
  public void shouldTellIfGivenCoordinateIsNotMine() throws MineOutOfBoardException {
    Board board = new Board(3, new Coordinate(2,2));
    assertTrue(board.notMine(new Coordinate(2,1)));
  }

  @Test(expected = MineOutOfBoardException.class)
  public void shouldThrowIfMineIsPlaceOutsideBoard() throws MineOutOfBoardException {
    new Board(2, new Coordinate(1,1), new Coordinate(2,1));
  }

  @Test
  public void shouldTellNumberOfNeighbourMinesInBoardOfSize3() throws MineOutOfBoardException {
    Board board = new Board(3, new Coordinate(1, 1), new Coordinate(2, 2));
    assertEquals(2, board.open(new Coordinate(2,1)));
  }

  @Test
  public void shouldTellNumberOfNeighbourMinesInBoardOfSize2() throws MineOutOfBoardException {
    Board board = new Board(2, new Coordinate(1, 1));
    assertEquals(1, board.open(new Coordinate(0,1)));
  }

  @Test
  public void shouldNotBlowUpIfIFlagAMine() throws MineOutOfBoardException {
    Board board = new Board(2, new Coordinate(1,1));
    assertEquals('m', board.flag(new Coordinate(1,1)));
  }

  @Test
  public void shouldTellNeighbouringMinesIfFlagIsNotMine() throws MineOutOfBoardException {
    Board board = new Board(2, new Coordinate(1,1));
    assertEquals(1, board.flag(new Coordinate(1,2)));
  }

  @Test
  public void shouldTellNumberOfMovesMade() throws MineOutOfBoardException {
    Board board = new Board(3, new Coordinate(2,2), new Coordinate(1,2), new Coordinate(0,0));
    board.open(new Coordinate(1,1));
    board.flag(new Coordinate(2,1));
    board.open(new Coordinate(0,1));
    assertEquals(3, board.numberOfMoves());
  }

  @Test
  public void shouldTellIfWonTheGame() throws MineOutOfBoardException {
    Board board = new Board(2, new Coordinate(1,1));
    board.open(new Coordinate(0,0));
    board.flag(new Coordinate(0,1));
    board.open(new Coordinate(1,1));
    assertTrue(board.won());
  }

  @Test
  public void shouldTerminateOnWiningMove() throws MineOutOfBoardException {
    Board board = new Board(2, new Coordinate(1,1));
    board.open(new Coordinate(0,0));
    board.flag(new Coordinate(0,1));
    assertEquals('w', board.open(new Coordinate(1,1)));
  }

  @Test
  public void shouldTerminateOnWiningMoveWhenIFlagAMine() throws MineOutOfBoardException {
    Board board = new Board(2, new Coordinate(1,1));
    board.open(new Coordinate(0,0));
    board.flag(new Coordinate(0,1));
    board.flag(new Coordinate(1,1));
    assertEquals('w', board.open(new Coordinate(1,1)));
  }
}
