package pathashala51;

import java.util.HashSet;

public class Board {
  private int size;
  private HashSet<Coordinate> mines;
  private HashSet<Coordinate> flagged;
  private HashSet<Coordinate> opened;

  public Board(int size, Coordinate... mines) throws MineOutOfBoardException {
    this.size = size;
    this.mines = new HashSet<>();
    this.flagged = new HashSet<>();
    this.opened = new HashSet<>();

    for (Coordinate mine: mines) {
      if(! mine.isInside(size)){
        throw new MineOutOfBoardException();
      }
      this.mines.add(mine);
    }
  }

  public boolean notMine(Coordinate coordinate) {
    return ! mines.contains(coordinate);
  }

  public int open(Coordinate coordinate) {
    if(!coordinate.isInside(size)) {
      return 0;
    }
    opened.add(coordinate);

    if(! notMine(coordinate)){
      return 'l';
    }

    if(won()){
      return 'w';
    }

    return getNeighbourMinesCount(coordinate);
  }

  private int getNeighbourMinesCount(Coordinate coordinate) {
    int neighbourMinesCount = 0;
    for (Coordinate neighbour: coordinate.neighbours()) {
      if(mines.contains(neighbour)){
        neighbourMinesCount++;
      }
    }
    return neighbourMinesCount;
  }

  public int flag(Coordinate coordinate) {
    flagged.add(coordinate);

    if(won()){
      return 'w';
    }

    if(mines.contains(coordinate)){
      return 'm';
    }
    return getNeighbourMinesCount(coordinate);
  }

  public int numberOfMoves() {
    return opened.size() + flagged.size();
  }

  public boolean won() {
    return (size*size) - (opened.size() + flagged.size()) < mines.size();
  }
}

