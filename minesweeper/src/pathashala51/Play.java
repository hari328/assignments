package pathashala51;

import java.util.Scanner;

public class Play {
  public static void main(String[] args){

    Board board;

    try {
      board = new Board(3,new Coordinate(1,1),new Coordinate(2,2), new Coordinate(0,0), new Coordinate(0,2));
    } catch (MineOutOfBoardException e) {
      System.out.println("mine place out of Board invalid input");
      return;
    }

    System.out.println("let's play");

    int previousMove = 0;

    while(previousMove != 'w'){
      System.out.println("Enter o(x,y) or f(x,y)");
      Scanner input = new Scanner(System.in);
      String nextLine = input.nextLine();
      boolean isOpen = false;

      if(nextLine.charAt(0) == 'o'){
        isOpen = true;
      }

      int xCoordinate = converCharToIntAtPos(nextLine, 2);
      int yCoordinate = converCharToIntAtPos(nextLine, 4);

      if(isOpen){
        previousMove = board.open(new Coordinate(xCoordinate,yCoordinate));
        if(previousMove == 'l'){
          System.out.println("Exploded");
          return;
        }
      }
      else{
        previousMove = board.flag(new Coordinate(xCoordinate,yCoordinate));
      }

    }

    System.out.println("That's The Way boy!!");

  }

  private static int converCharToIntAtPos(String nextLine, int index) {
    return (int) nextLine.charAt(index) - 48;
  }
}
