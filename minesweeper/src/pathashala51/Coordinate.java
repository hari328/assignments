package pathashala51;

public class Coordinate {
  private final int xCoordinate;
  private final int yCoordinate;

  public Coordinate(int x, int y) {
    xCoordinate = x;
    yCoordinate = y;
  }

  public boolean isInside(int sizeOfSquareBoard) {
    return (xCoordinate < sizeOfSquareBoard && yCoordinate < sizeOfSquareBoard);
  }

  public Coordinate[] neighbours() {
      return new Coordinate[]{this.left(), this.right(), this.top(), this.bottom() };
  }

  private Coordinate bottom() {
    return new Coordinate(xCoordinate, yCoordinate-1);
  }

  private Coordinate top() {
    return new Coordinate(xCoordinate, yCoordinate+1);
  }

  private Coordinate right() {
    return new Coordinate(xCoordinate+1, yCoordinate);
  }

  private Coordinate left() {
    return new Coordinate(xCoordinate-1,yCoordinate);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Coordinate that = (Coordinate) o;

    if (xCoordinate != that.xCoordinate) return false;
    return yCoordinate == that.yCoordinate;

  }

  @Override
  public int hashCode() {
    int result = xCoordinate;
    result = 31 * result + yCoordinate;
    return result;
  }
}
