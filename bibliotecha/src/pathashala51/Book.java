package pathashala51;

import java.time.Year;

public class Book {
  private String name;
  private String author;
  private Year yearOfPublising;

  public Book( String name, String author, Year yearOfPublishing) {
    this.name = name;
    this.author = author;
    this.yearOfPublising = yearOfPublishing;
  }


  @Override
  public String toString() {
    return "Book{" +
        "Name='" + name + '\'' +
        ", Author='" + author + '\'' +
        ", Year=" + yearOfPublising +
        '}';
  }
}
