package pathashala51;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Library {

  private ArrayList<Book> allBooks;

  public Library(ArrayList<Book> booksAdded) {
    this.allBooks = booksAdded;
  }

  public String welcomeMessage() {
    return "Welcome To Biblioteca";
  }

  //can we test this using stubs.
  public ArrayList<Book> allBooks() {
    return allBooks;
  }

  public List<Book> selectMenu(Menu MenuOption) {
    if(MenuOption == Menu.ALL_BOOKS){
      return allBooks;
    }
    else if(MenuOption == Menu.EXIT){
      System.exit(0);
    }
    return Collections.emptyList();
  }

  enum Menu{
    ALL_BOOKS,
    NEW_BOOKS,
    OLD_BOOK,
    EXIT
  }
}
