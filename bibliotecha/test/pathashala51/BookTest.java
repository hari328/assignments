package pathashala51;

import org.junit.Test;

import java.time.Year;

import static junit.framework.Assert.assertEquals;

public class BookTest {

  @Test
  public void shouldBeAbleToDisplayBookInformation(){
    Book book = new Book("Book One", "Jake", Year.of(2015));
    assertEquals("Book{Name='Book One', Author='Jake', Year=2015}",book.toString());
  }
}