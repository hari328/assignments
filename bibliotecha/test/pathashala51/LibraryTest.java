package pathashala51;

import org.junit.Test;

import java.time.Year;
import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

public class LibraryTest {

  @Test
  public void shouldAbleToSeeWelcomeMessage(){
    assertEquals("Welcome To Biblioteca", new Library(null).welcomeMessage());
  }

  @Test
  public void shouldShowListOfBooksInLibrary(){
    ArrayList<Book> booksAdded = new ArrayList<>();
    booksAdded.add(new Book("Book 1", "Jake", Year.of(1995)));
    booksAdded.add(new Book("Book 2", "Jake", Year.of(1993)));
    final Library library = new Library(booksAdded);

    assertEquals(booksAdded, library.allBooks());
  }

  @Test
  public void shouldDisplayAllBooksIfAllBooksOptionSelectedInMainMenu(){
    ArrayList<Book> booksAdded = new ArrayList<>();
    booksAdded.add(new Book("Book 1", "Jake", Year.of(1995)));
    booksAdded.add(new Book("Book 2", "Jake", Year.of(1993)));
    final Library library = new Library(booksAdded);

    assertEquals(booksAdded, library.selectMenu(Library.Menu.ALL_BOOKS));
  }

}
