package pathashala51;

//represents imported goods on which local taxes are also apply

public class ImportedTaxableCommodity extends TaxableCommodity {

  private final int IMPORTING_TAX_PERCENTAGE = 5;

  public ImportedTaxableCommodity(float price) {
    super(price);
  }

  @Override
  public float sellingPrice(){
    return super.sellingPrice()+ tax(IMPORTING_TAX_PERCENTAGE);
  }
}
