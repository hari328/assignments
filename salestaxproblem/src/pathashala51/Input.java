package pathashala51;

import java.util.ArrayList;
import java.util.List;

public class Input {
  private List<Commodity> commodityList;

  public Input(List<Commodity> inputList) {
    commodityList = inputList;
  }

  //32.19 20.89 9.75 11.85
  public float sellingPrice(){
    float sellingPrice = 0;
    for (Commodity commodity: commodityList) {
      sellingPrice = Utility.add(sellingPrice,commodity.sellingPrice());
    }
    return sellingPrice;
  }

  public float costPrice() {
    float totalPrice = 0;
    for (Commodity commodity: commodityList) {
      totalPrice = Utility.add(totalPrice,commodity.price());
    }
    return totalPrice;
  }

  public float salesTax() {
    return Utility.roundOf(sellingPrice() - costPrice());
  }

  public float[] printIndividualSellingPrices(){
    float[] result = new float[commodityList.size()];
    for (int i = 0; i < commodityList.size() ; i++) {
      result[i] = commodityList.get(i).sellingPrice();
    }
    return result;
  }

  @Override
  public String toString() {
    return "Input{" +
        "commodityList=" + commodityList +
        '}';
  }

  static class CommodityBuilder {

    private ArrayList<Commodity> inputList;
    public CommodityBuilder() {
      this.inputList = new ArrayList<>();
    }

    public Input build() {
      Input input = new Input(this.inputList);
      return input;
    }

    public CommodityBuilder add(Commodity commodity) {
      inputList.add(commodity);
      return this;
    }

  }
}
