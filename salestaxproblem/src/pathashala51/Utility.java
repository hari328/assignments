package pathashala51;

public class Utility {
  public static float roundOf(float tax) {
    if(! shouldRoundOf(tax)){
      return tax;
    }

    int value = (int)(tax*100);
    int lastNumber = value%10;
    if(lastNumber <= 2){
      return (float)(value - lastNumber)/100;
    }
    if(lastNumber > 2 && lastNumber <=7){
      return (float)(value - lastNumber + 5)/100;
    }
    return (float)(value - lastNumber +10)/100;
  }

  private static boolean shouldRoundOf(float tax) {
    tax = tax*100;
    return tax % 100 != 0;
  }

  public static float add(float valueOne, float valueTwo ) {
    valueOne = valueOne*100;
    valueTwo = valueTwo*100;
    return (valueOne + valueTwo) / 100;
  }
}
