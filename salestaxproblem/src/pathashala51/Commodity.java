package pathashala51;

// represents "not imported" good and "no tax" applied

public class Commodity {
  private float price;

  public Commodity(float price) {
    this.price = price;
  }


  public float sellingPrice(){
    return price;
  }

  protected float tax(int taxPercentage) {
    float taxCalculated = (price / 100)*taxPercentage;
    return Utility.roundOf(taxCalculated);
  }

  @Override
  public String toString() {
    return "Commodity{" +
        "price=" + price +
        '}';
  }

  public float price() {
    return this.price;
  }
}
