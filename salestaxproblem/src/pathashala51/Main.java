package pathashala51;

import java.util.Arrays;

import static pathashala51.Input.*;

public class Main {
  public static void main(String[] args){
    CommodityBuilder builder = new CommodityBuilder();
    Input inputOne = builder.add(new Commodity(12.49f)).add(new TaxableCommodity(14.99f)).add(new Commodity(0.85f)).build();
    builder = new CommodityBuilder();
    Input inputTwo = builder.add(new ImportedCommodity(10.00f)).add(new ImportedTaxableCommodity(47.50f)).build();
    builder = new CommodityBuilder();
    Input inputThree = builder.add(new ImportedTaxableCommodity(27.99f)).add(new TaxableCommodity(18.99f)).
        add(new Commodity(9.75f)).add(new ImportedCommodity(11.25f)).build();

    for (Input input: Arrays.asList(inputOne, inputTwo, inputThree)) {
      System.out.println("Input: ");
      System.out.println(input);

      System.out.println("Output: ");
      print(input.printIndividualSellingPrices());
      System.out.println("Sales tax: "+ input.salesTax());
      System.out.println("Total: "+input.sellingPrice());
      System.out.println();
    }
  }

  private static void print(float[] individualSellingPrices) {
    for (float value: individualSellingPrices) {
      System.out.println(value);
    }
  }
}


