package pathashala51;

//represents non imported goods on which local taxes are applied.

public class TaxableCommodity extends Commodity {
  private final int TAX_PERCENTAGE = 10;

  public TaxableCommodity(float price) {
    super(price);
  }

  @Override
  public float sellingPrice(){
    return super.sellingPrice() + tax(TAX_PERCENTAGE);
  }

}
