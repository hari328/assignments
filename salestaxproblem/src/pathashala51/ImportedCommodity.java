package pathashala51;

//represents "Imported" goods on which "NO" local taxes apply

public class ImportedCommodity extends Commodity {

  private final int IMPORTING_TAX_PERCENTAGE = 5;

  public ImportedCommodity(float price) {
    super(price);
  }

  @Override
  public float sellingPrice(){
    return super.sellingPrice() + tax(IMPORTING_TAX_PERCENTAGE);
  }
}
