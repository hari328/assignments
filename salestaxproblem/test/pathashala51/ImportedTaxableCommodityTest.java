package pathashala51;

import org.junit.Assert;
import org.junit.Test;

public class ImportedTaxableCommodityTest {

  @Test
  public void shouldCalculateSellingPriceForImportedTaxableCommodity(){
    ImportedTaxableCommodity commodity = new ImportedTaxableCommodity(27.99f);
    float expectedSellingPrice = 32.19f;
    Assert.assertEquals(expectedSellingPrice, commodity.sellingPrice(), 0.0f);
  }
}