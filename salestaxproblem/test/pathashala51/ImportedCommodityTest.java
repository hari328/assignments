package pathashala51;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ImportedCommodityTest {
  @Test
  public void shouldCalculateSalesPriceOfImportedCommodity(){
    ImportedCommodity commodity = new ImportedCommodity(10.0f);
    float expectedSellingPrice = 10.50f;
    assertEquals(expectedSellingPrice, commodity.sellingPrice(),0.0f);
  }

  @Test
  public void shouldCalculateSalesPriceOfImportedCommodityBreakingCase(){
    ImportedCommodity commodity = new ImportedCommodity(11.25f);
    float expectedSellingPrice = 11.80f;
    assertEquals(expectedSellingPrice, commodity.sellingPrice(),0.0f);
  }
}