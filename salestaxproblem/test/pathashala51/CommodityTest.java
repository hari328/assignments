package pathashala51;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CommodityTest {

  @Test
  public void shouldNotIncludeTaxInSellingPriceIsNonZero(){
    final float sellingPrice = 12.49f;
    Commodity commodity = new Commodity(sellingPrice);
    assertEquals(sellingPrice, commodity.sellingPrice());
  }

  @Test
  public void shouldNotIncludeTaxInSellingPriceIsZero(){
    final float sellingPrice = 0f;
    Commodity commodity = new Commodity(sellingPrice);
    assertEquals(sellingPrice, commodity.sellingPrice());
  }

}