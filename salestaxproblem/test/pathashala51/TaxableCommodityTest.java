package pathashala51;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TaxableCommodityTest {

  @Test
  public void computeSalePriceOfTaxableCommodity(){
    Commodity commodity = new TaxableCommodity(14.99f);
    float expectedSellingPrice = 16.49f;
    assertEquals(expectedSellingPrice, commodity.sellingPrice());
  }

  @Test
  public void salesTaxOfToLowerNumber(){
    Commodity commodity = new TaxableCommodity(18.99f);
    float expected = 20.89f;
    assertEquals(expected, commodity.sellingPrice());
  }

  @Test
  public void taxShouldRoundedToNearestToFiveMultiple(){
    Commodity commodity = new TaxableCommodity(45.66f);
    float expected = 4.55f;
    assertEquals(expected, commodity.tax(10));
  }

  @Test
  public void taxShouldRoundedToNearestToPreviousNumber(){
    Commodity commodity = new TaxableCommodity(45.26f);
    float expected = 4.50f;
    assertEquals(expected, commodity.tax(10));
  }

  @Test
  public void taxShouldBeRoundedToNextNumber(){
    Commodity commodity = new TaxableCommodity(34.88f);
    float expected = 3.50f;
    assertEquals(expected, commodity.tax(10));
  }

  @Test
  public void testShouldBeRoundedToNearestFive(){
    Commodity commodity = new Commodity(66.77f);
    float expected = 6.65f;
    assertEquals(expected, commodity.tax(10));
  }


}
