package pathashala51;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static pathashala51.Input.CommodityBuilder;

public class InputTest {

  @Test
  public void createInputListOf3Commodities() {
    CommodityBuilder builder = new CommodityBuilder();
    Input commodities = builder.add(new Commodity(34f)).add(new Commodity(12)).add(new Commodity(1f)).build();
    assertNotNull(commodities);
  }

  @Test
  public void costPriceOfInputCommodities() {
    CommodityBuilder builder = new CommodityBuilder();
    Input commodities = builder.add(new Commodity(34f)).add(new Commodity(12)).add(new Commodity(1f)).build();
    assertEquals(47f, commodities.costPrice());
  }

  @Test
  public void sellingPriceOfInputCommodities() {
    CommodityBuilder builder = new CommodityBuilder();
    Input inputOne = builder.add(new Commodity(12.49f)).add(new TaxableCommodity(14.99f)).add(new Commodity(0.85f)).build();
    assertEquals(29.83f, inputOne.sellingPrice());
  }

  @Test
  public void salesTaxValueForInputList() {
    CommodityBuilder builder = new CommodityBuilder();
    Input input = builder.add(new ImportedTaxableCommodity(27.99f)).add(new TaxableCommodity(18.99f)).
        add(new Commodity(9.75f)).add(new ImportedCommodity(11.25f)).build();
    assertEquals(6.65,input.salesTax(),0.01f);
  }

}
